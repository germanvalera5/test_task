from django.db import models
from django.core.validators import RegexValidator


class Person(models.Model):
    first_name = models.CharField(max_length=100, unique=True)
    last_name = models.CharField(max_length=100, unique=True)
    country = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    street = models.CharField(max_length=100, null=True, blank=True)
    url_address = models.URLField(null=True, blank=True)
    phone_number_regex = RegexValidator(regex=r'^\+?1?\d{8,15}$')
    phone_number = models.CharField(validators=[phone_number_regex],
                                    max_length=16, unique=True, blank=True, null=True)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return f'{self.last_name} {self.first_name}'

