from django.shortcuts import render, redirect
from django.db.models import Q
from django.views import generic

from address_book.forms import FilterForm
from address_book.models import Person


class AddressBookList(generic.ListView):
    model = Person
    template_name = 'address_book/base.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AddressBookList, self).get_context_data(**kwargs)
        context['form'] = FilterForm
        return context

    @staticmethod
    def post(request):
        form = FilterForm(request.POST)
        return redirect('address_book:home-filtered', mystr=form.data['text'])


def get_filter_book(request, mystr):
    queryset = Person.objects.filter(
        Q(first_name__contains=mystr) | Q(last_name__contains=mystr) |
        Q(country__contains=mystr) | Q(city__contains=mystr) |
        Q(street__contains=mystr) | Q(url_address__contains=mystr)
    )
    if not queryset:
        message = f'Ничего не найдено по запросу "{mystr}".'
        return render(request, 'address_book/filter_search.html', context={'message': message})
    return render(request, 'address_book/filter_search.html', context={'object_list': queryset})


class OnePerson(generic.DetailView):
    model = Person
    template_name = 'address_book/one_person.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        pk = self.kwargs.get(self.pk_url_kwarg)
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            obj = "Запись не найдена."
            return obj
        return obj


class OnePersonCreate(generic.CreateView):
    model = Person
    template_name = 'address_book/create_person.html'
    fields = ('first_name', 'last_name', 'country', 'city', 'street',
              'url_address', 'phone_number', 'image')
    success_url = '/'


class OnePersonEdit(generic.UpdateView):
    model = Person
    template_name = 'address_book/edit_person.html'
    fields = ('first_name', 'last_name', 'country', 'city', 'street',
              'url_address', 'phone_number', 'image')
    success_url = '/'


class OnePersonDelete(generic.DeleteView):
    model = Person
    template_name = 'address_book/confirm_delete.html'
    success_url = '/'
