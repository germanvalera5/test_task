from django.urls import path, re_path

from address_book import views
from address_book.views import AddressBookList, OnePerson, OnePersonCreate, OnePersonEdit, OnePersonDelete

app_name = 'address_book'

urlpatterns = [
    path('', AddressBookList.as_view(), name='home'),
    re_path(r'^filtered/(?P<mystr>.+)/$', views.get_filter_book, name='home-filtered'),
    path('person/<int:pk>/', OnePerson.as_view(), name='person-view'),
    path('person_edit/<int:pk>/', OnePersonEdit.as_view(), name='person-edit'),
    path('create/', OnePersonCreate.as_view(), name='person-create'),
    path('person_delete/<int:pk>/', OnePersonDelete.as_view(), name='person-delete')
]