from django import forms


class FilterForm(forms.Form):
    text = forms.CharField(max_length=100, required=True)
